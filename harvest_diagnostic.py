
import sys
import os
import subprocess
import time
from itertools import cycle

about = """
    Harvest Diagnostic.

    Summarizes and analyzes avg_latency and total_ops counters of an SVM or Node
    in whisper database. Highlights with red if they are incorrect and shows
    correct values.

    When run on an SVM, diagnostics is also done on subvolumes.

    REQUIREMENTS:
        Python 2
        whisper_fetch.py

    USAGE:
        $ python3 harvest_diagnostic.py <path> <options>

        path:       Full path to SVM or Node
        options:
            -h      Print this help text
            -v      Verbose, print more output
            -f      Filter empty lines (where values are 0s)
            -t      Limit counters to last N s[ec], m[in], h[our], [d]ay, w[eek]

        Example:

        $ python3 /opt/.../svm/svm1 -vf -t=10h


    AUTHOR: Vachagan Gratian, [vachagan//netapp.com], NetApp EMEA.
    This is an package and comes with no warranty.

"""

latency_counters = ('read_latency', 'write_latency', 'other_latency', 'avg_latency')
ops_counters =  ('read_ops', 'write_ops', 'other_ops', 'total_ops')

wfetch = 'whisper-fetch.py'
wfrom = None
wtime = 24
wtunit = 'h'

spinner = cycle(['/', '-', '\\', '|'])

path = None
verbose = False
filtr = False

# Used for output decoration
END = '\033[0m'
BOLD = '\033[1m'
ITALIC = '\033[3m'
RED = '\033[91m'
GREEN = '\033[92m'


def main():
    """
    Main routine. Gets data from SVM/Node and subvolumes.
    """

    parse_args()
    check_dependencies()

    global path, spinner

    check_counters(path+'vol_summary/')

    # Do calculations for SVM or Node
    print('Summarizing SVM/Node counters in [vol_summary].')
    svm_summary = get_counter_summaries(path+'vol_summary/', path.split('/')[-2])
    print('OK.')

    # Do the same for subvolumes
    path += 'vol/'
    volumes = []
    if os.path.exists(path):
        volumes = [v for v in os.listdir(path) if os.path.isdir(path + v)]

        if volumes:
            summaries = []
            print('Summarizing counters of {} volumes.'.format(len(volumes)))
            vprint()
            for vol in volumes:
                spin()
                summaries.append(get_counter_summaries(path+vol+'/', vol))
            print('OK.')

    # Show results
    print_header()
    print_values(svm_summary, path.split('/')[-3].upper(), bold=True)

    if volumes:
        for vol, values in zip(volumes, summaries):
            print_values(values, vol)

    sys.exit(0)


def get_counter_summaries(path, name=None):
    """
    Reads whisper files from given path. Summarizes counters. Recalculates the
    avg_latency and total_ops counters.

    ARGUMENTS:
        path - STRING: full path to folder with counters
        name - STRING: name of volume/svm/node (can also be extracted from path)

    RETURNS:
        S   - DICT: 8 items, of which 6 are the summarized counters and 2 are
              estimated _avg_latency and _total_ops.

        If some of the counter files were missing, they'll be missing in S too.
        If not counters were imported, you'll get an empty dict (keys, but no
        values).

    Counters are imported from 6 files:
    - read_latency.wsp
    - write_latency.wsp
    - other_latency.wsp
    - read_ops.wsp
    - write_ops.wsp
    - other_ops.wsp
    """

    name = path.split('/')[-1] if not name else name

    # Collect raw counter values from whisper files
    # R will be a dict of dicts: counter => timestamp => value (floats)
    # T will be collection of all available timestamps (strings)
    R = {c:{} for c in (latency_counters+ops_counters)}
    T = set()

    vprint()
    vprint('Reading whisper files from [{}].'.format(name))

    exist = pairs_exist(path)
    if not any(exist):
        vprint('WARNING: no read/write/other pairs found.')
    else:
        vprint('Read/write/other pairs: {}'.format(exist.count(True)))

    for counter in (latency_counters+ops_counters):
        data, _ = whisper_fetch(path, counter)

        spin()
        for entry in data:
            t, value = entry.strip().split()
            T.add(t)
            if value != 'None':
                R[counter][t] = float(value)

    T = sorted(list(T))
    # In verbose mode: print timespan and number of counters collected
    total_h = (int(T[-1]) - int(T[0])) / 3600 if T else 0
    vprint('Collected {} time entries from [{}] over the last {} hours.'.format \
        (len(T), name, round(total_h,1)))

    # Summarize all entries from last 24h into one value for each counter
    # S will be dict: counter => summed value (float)
    S = {c:0 for c in (latency_counters+ops_counters)}
    S['_avg_latency'] = 0
    S['_total_ops'] = 0
    # "Clean" sums of OPs values, i.e. only those with Latency pairs
    C = {c:0 for c in ops_counters}
    C['_total_ops'] = 0

    # Do weighted sum for latencies and raw sum for OPs
    for t in T:
        spin()
        for latency, ops in zip(latency_counters, ops_counters):
            if t in R[ops]:
                S[ops] += R[ops][t]
                if ops != 'total_ops':
                    S['_total_ops'] += R[ops][t]

            if t in R[ops] and t in R[latency]:
                S[latency] += (R[latency][t] * R[ops][t])
                C[ops] += R[ops][t]
                # Add read, write and other latencies to _avg_latency
                if latency != 'avg_latency': #and any(exist):
                    S['_avg_latency'] += (R[latency][t] * R[ops][t])
                    C['_total_ops'] += R[ops][t]


    # Normalize latencies with correspondig OPs value
    for latency, ops in zip(latency_counters, ops_counters):
        if S[latency]:
            S[latency] = S[latency] / C[ops] if C[ops] else 'N/A'
        else:
            S[latency] = 'N/A'

    # Normalize _avg_latency with _total_ops
    if any(exist):
        S['_avg_latency'] = S['_avg_latency']/C['_total_ops'] if C['_total_ops'] else 'N/A'
    else:
        vprint('Estimated values set to N/A.')
        S['_avg_latency'] = 'N/A'
        S['_total_ops'] = 'N/A'


    vprint('{}{}{}'.format(BOLD, '\n'.join(sorted([str(v) for v in S.items()])), END))

    return S


def whisper_fetch(path, counter, t=None):
    """
    Import counter values of the last 24 hours (usually 1440 values in total).
    Skip values that are "None".

    ARGUMENTS:
        path    - STRING: path to directory whith the counter file
        counter - STRING: name of the counter

    OPTIONS:
        wfrom   - If set, will fetch data from this time

    RETURNS:
        data    - LIST of STRINGs: each string is a pair of timestamp and value
                  seperated by tab.
                  If nothing was imported, data is empty list.
        exists  - BOOL: does the file exist or not.
    """

    data = []
    exists = False

    if os.path.exists(path+counter+'.wsp'):
        exists = True
        cmd = '{} {}{}.wsp --drop=empty'.format(wfetch, path, counter)
        if wfrom:
            cmd += ' --from={}'.format(wfrom)
        elif t:
            cmd += ' --from={}'.format(t)
        try:
            p = subprocess.Popen(   cmd,
                                    shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE )
            try:
                #  Throws TimeoutExpired after timeout
                p.wait(timeout=0.5)
            except:
                pass
            result, err = p.communicate()
        except:
            pass
        else:
            data = result.decode().splitlines()
    return data, exists


def pairs_exist(path):
    """
    Checks how many latency-ops pairs exist (read, write or other).

    ARGUMENTS:
        path - STRING: full path to folder with counters

    RETURNS:
        TUPLE of 3 elements, each:
               BOOL: pair exists or not
    """
    exists = []
    vprint('Checking Latency-OPs pairs in [{}].'.format(path.split('/')[-2]))
    for lat, ops in zip(latency_counters[:-1], ops_counters[:-1]):
        if os.path.exists(path+lat+'.wsp') and os.path.exists(path+lat+'.wsp'):
            exists.append(True)
            vprint('-> {} + {}: OK'.format(lat, ops))
        else:
            exists.append(False)
            vprint('-> {} + {}: MISSING'.format(lat, ops))
    return tuple(exists)


def print_header():
    """
    Prints labels and some dashes to make it look like a table.
    First field is 25 chars long, all the others: 15.

    Note: print_values() should be consistent with these sizes.

    """

    print('\n{}Average Latency and Total OPs of the last {} {}.\nEstimated' \
        ' values are marked with *.{}\n'.format(BOLD, wtime, wtunit, END))

    labels = (
                'SVM / Node / Volume',
                'Avg Latency',
                '*Avg Latency',
                'Total OPs',
                '*Total OPs',
                )

    header = ('-'*25) + ' ' + (('-' * 15 + ' ') * 4) + '\n'

    header += (labels[0] + ( ' ' * (25 - len(labels[0])) ) + ' ')
    for l in labels[1:]:
        header += ( (' ' * (15 - len(l)) ) + l + ' ' )
    header += ( '\n' + ('-'*25) + ' ' + (('-' * 15 + ' ') * 4) )
    print(header)


def print_values(values, name, bold=False):
    """
    Prints values on a single line. Optionally skips all-zero line (if
    global filtr is set True).

    ARGUMENTS:
        values  - DICT: where key is counter (STRING) and value is FLOAT.
        name    - STRING: name of volume
        bold    - BOOL: print everying in bold

    """

    # Optionally skip if all values are 0s
    empty = [False if x and x!='N/A' else True for x in values.values()]
    if filtr and all(empty):
        return
    if values['_avg_latency'] == 'N/A' and values['_total_ops'] == 'N/A':
        return

    counters = ('avg_latency', '_avg_latency', 'total_ops', '_total_ops')
    results = ''

    # Add whitespace for left aligning
    if len(name) > 25:
        name = name[:22]+'.. '
    elif len(name) < 25:
        name += (' ' * (25-len(name)) )

    # Optionally print in bold
    if bold:
        name = BOLD + name + END
    results += (name + ' ')

    # Preprocess and print values
    for c in counters:
        consistent = 0
        if values[c] == 'N/A':
            v = 'N/A'
        else:
            v = round(values[c],2)

            # Compare original and corrected values
            if c[0] != '_' and v != 0:
                _c = '_' + c
                if type(values[_c]) is not str:
                    consistent = check_diff(round(v,2), round(values[_c],2))
            v = str(v)

        # Add whitespace right aligning
        if len(v) < 15:
            v = (' ' * (15-len(v))) + v

        # Color-highlight difference between original and corrected values
        if consistent == 1:
            v = GREEN + v + END
        elif consistent == -1:
            v = RED + v + END

        # Optionally print in bold
        if bold:
            v = BOLD + v + END

        results += ( v + ' ')

    print(results)


def parse_args():
    """
    Parse command line arguments: path and options. Test if path is valid.
    """
    # Chech minimal number of arguments
    if len(sys.argv) < 2:
        print('\nPlease provide path to SVM/Node. Exiting.\n')
        print_help()
        sys.exit(1)

    # First argument should be path to SVM
    global path
    path = sys.argv[1] if sys.argv[1][-1] == '/' else sys.argv[1]+'/'

    # Second argument should be option(s)
    if len(sys.argv) > 2:

        # timeperiod is specified (since t)
        if '-t=' in sys.argv[-1]:

            t_units = { 's': 1, 'm': 60, 'h': 3600, 'd': 86400, 'w': 604800}

            global wfrom, wtime, wtunit

            wtime = sys.argv[-1].split('=')[1]

            if wtime[-1] not in t_units:
                print('Unrecognized time unit in [{}]. Expected one of [{}]' \
                    ' at the end.'.format(wtime, ' '.join(t_units.keys())))
                sys.exit(2)
            else:
                wtunit = wtime[-1]
                try:
                    wtime = int(wtime[:-1])
                except:
                    print('Unexpected time value [{}]. Expected integer ' \
                        'with one of [{}].'.format(wtime),' '.join(t_units))
                    sys.exit(2)
                else:
                    wfrom = int( time.time() - (wtime * t_units[wtunit]) )

            options = ''.join(sys.argv[2:-1])
        else:
            options = ''.join(sys.argv[2:])

        if 'h' in options:
            print_help()
            sys.exit(1)

        global filtr, verbose

        if 'f' in options:
            filtr = True
        if 'v' in options:
            verbose = True

    # Verify path exists

    print('Checking SVM/Node path [.../{}].'.format(path.split('/')[-2]))
    if not path or not os.path.exists(path):
        print('FAILED.\nInvalid path [{}]'.format(path))
        sys.exit(1)

    if not os.path.exists(path+'vol_summary/'):
        print('FAILED.\nNo [vol_summary] folder found. Are you sure ' \
            'path is valid?')
        sys.exit(1)

    print('OK')


def check_diff(a, b):
    """
    Checks 2 float values. Returns 1 if they differ by a margin of ~2%, else -1.
    """
    return 1 if b==a or b+a==0 or abs((b-a)/(b+a))<0.01 else -1


def check_dependencies():
    """
    Check if whisper-fetch.py is available in shell.
    """

    print('Checking dependency [{}].'.format(wfetch))

    try:
        p = subprocess.Popen( wfetch,
                              shell=True,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE )
        p.wait()
        outp, err = p.communicate()
    except:
        print('FAILED\nUnable to run [{}] in shell. Exiting.'.format(wfetch))
        sys.exit(1)
    else:
        if not outp and err:
            print('FAILED\n[{}] required to run this script. Exiting.'.format(
                wfetch))
            sys.exit(1)
        print('OK.')


def check_counters(path):
    """
    Checks if at least one latency-ops pairs exist (read, write or other). If
    not asks weither to continue or not.

    ARGUMENTS:
        path - STRING: full path to folder with counters
    """
    exists = pairs_exist(path)

    if not any(exists):
        print('WARNING: Not enough counters in [vol_summary]. Estimated ' \
            'values will be incorrect.')
        answer = raw_input('Continue anyway? [yes/NO]: ')
        if answer.lower() != 'yes':
            sys.exit(0)
        else:
            return
    else:
        vprint('Done, {} counter pairs found.\n'.format(exists.count(True)))


def vprint(msg='', end='\n'):
    """
    Print message if verbose mode is on.
    """
    if verbose:
        print(msg)


def spin():
    """
    Print rotating dash.
    """
    if not verbose:
        global spinner
        sys.stdout.write(next(spinner))
        sys.stdout.flush()
        sys.stdout.write('\b')


def print_help():

    print(about)


if __name__ == '__main__':
    main()
