
# Harvest Diagnostic

This tool analyzes Latency and IOPS counters in a Whisper database. Those counters were collected by NetApp Harvest and in case of a ONTAP 9.4 system, they can be abnormally high.

The tool compares `avg_latency` and `total_ops` to their estimated values (calculated from read, write and other latencies/IOPS). Matching values are highlighted in green, inaccurate values are highlighted in red. Note that there might be still a difference between actual and estimated values, since `avg_latency` and `total_ops` often include indirect operations.

## Requirements

  * Python2 or higher
  * Whisper and [whisper-fetch.py](https://github.com/graphite-project/whisper#whisper-fetchpy)

Both of this requirements are usually already satisfied on any server that runs Harvest.

## Usage

Download `harvest_diagnostic.py` to any location on the Harvest/Whisper server and run:
```sh
$ python3 harvest_diagnostic.py <path> <options>
```
The required argument is the full path to an SVM or Node in the Whisper database. Optional arguments are:

* `-h` - print help text
* `-f` - filter empty lines (where values are 0s)
* `-v` - verbose, print more output
* `-t=` - limit timespan to the last *n* **s**ec, **m**in, **h**ours, **d**ays, **w**-eeks (default is last 24 hours)

Example:

```sh
$ python3 harvest_diagnostic.py /opt/graphite/storage/whisper/netapp/perf/EVN/cuba/node/cuba-04 -vf -t=24h
```

### Output
Example output (colors are here not displayed):


```sh
Checking SVM path [.../svm_vachagan_CIFS]. OK
Checking dependency [whisper-fetch.py]. OK.
Calculating averages of 10 volumes. OK.

Average Latency and Total OPs of the last 24 h.
Estimated values are marked with *.

------------------------- --------------- --------------- --------------- ---------------
SVM / Node / Volume           Avg Latency    *Avg Latency       Total OPs      *Total OPs
------------------------- --------------- --------------- --------------- ---------------
SVM_VACHAGAN_CIFS                    0.01            0.01         2425.81         2425.81
svm_vachagan_CIFS_flex..             0.02            0.02           13.51           13.51
svm_vachagan_CIFS_flex..             0.02            0.02           284.4           284.4
svm_vachagan_CIFS_flex..             0.02            0.02          150.95          150.95
svm_vachagan_CIFS_flex..             0.02            0.02          171.69          171.69
svm_vachagan_CIFS_vol0               0.02            0.02          130.74          130.74
svm_vachagan_CIFS_flex..             0.02            0.02          152.61          152.61
svm_vachagan_CIFS_flex..             0.02            0.02          176.22          176.22
svm_vachagan_CIFS_flex..             0.02            0.02          241.53          241.53
svm_vachagan_CIFS_flex..             0.02            0.02          256.84          256.84
```

The estimated (recalculated) values are in the columns marked with asterisks (\*) . If the actual values are correct, i.e. if they are close to the estimated ones, they will be highlighter in
green, otherwise in red.

All the Latency and OPs values displayed are averages and totals respectively of the last 24 hours .


## How are the estimated values calculated?

__\*Total OPs__ is calculated as the sum of the other OPs counters:

`total_ops` = ( `read_ops` + `write_ops` + `other_ops`)

__\*Avg Latency__ is calculated as the average of the other Latency counters weighted with their corresponding OPs counter:

`avg_latency` = ( (`read_latency` \* `read_ops`) + (`read_latency` \* `read_ops`) + (`other_latency` \* `other_ops`) ) / `total_ops`
